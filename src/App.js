import React from 'react';
import Home from "./pages/Home";
// import Detail from "./components/DetailItemComponent"
import MovieDetail from "./pages/MovieDetail";
import NotFoundPage from "./pages/404Page"
import { BrowserRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';
function App() {
    
  return (
    <div>

        <Router>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/detail/:imdbID" component= {MovieDetail}/>
                <Route path="*" component={NotFoundPage}/>
            </Switch> 
        </Router>
       
    </div>
  );
}

export default App;
