import React, {useState} from "react";

import {
    Card, CardImg, CardBody,
    CardTitle,CardSubtitle
} from 'reactstrap';

import Detail from "./DetailItemComponent";
const DEFAULT_IMAGE_ALT= "https://images-na.ssl-images-amazon.com/images/I/81qwPaPvCYL._SL1016_.jpg";
const Movie = ({movie}) => {
    const poster_movie = movie.Poster === "N/A" ? DEFAULT_IMAGE_ALT : movie.Poster

    return(
        <div className="movie">
            <Card body outline color="secondary">
                <CardImg top width="100%"
                         src={poster_movie}
                         alt={`This is poster of ${movie.Title}`}
                />
                <CardBody>
                    <CardTitle>{movie.Title}</CardTitle>
                    <CardSubtitle>Year: {movie.Year}</CardSubtitle>
                    <Detail movie={movie} />
                </CardBody>
            </Card>
        </div>
    )
}
export default Movie