import React, {useState} from 'react';

const Search = (props) => {
    const [searchValue, setSearchValue] = useState("")
    const handleInputChange = (e) => {
        setSearchValue(e.target.value)
    }
    const resetInputField = () => {
        setSearchValue("")
    }
    const callSearchFunction = (e) => {
        e.preventDefault();
        props.search(searchValue);
        resetInputField();
    }
    return(
        <form action="" className="search">
            <input
                value={searchValue}
                onChange={handleInputChange}
                type="text"
            />
            <input onClick={callSearchFunction} type="submit" value="Search"/>
        </form>
    )
}
export default Search