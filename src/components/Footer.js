import React from "react";
const Footer = (props) => {
    return (
        <header className="footer">
            <h2>{props.text}@movie</h2>
        </header>
    )
}
export default Footer