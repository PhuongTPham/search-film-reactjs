import React, {useState} from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

const Detail = ({movie}) => {
    const [openModal, setOpenModal] = useState(false)
    const [detailMovie, setDetailMovie] = useState({})

    const toggle = (e) => {
        e.preventDefault();
        setOpenModal(!openModal)
    }
    const clickHandle = () => {
        setOpenModal(true);
        fetch(`https://www.omdbapi.com/?i=${movie.imdbID}&apikey=bc1d4985`)
            .then(response => response.json())
            .then(jsonResponse => {
                if (jsonResponse.Response === "True") {
                    setDetailMovie(jsonResponse)
                } else {
                }
            });
    }
    return (
        <div className="detail">
            <Button color="danger" onClick={clickHandle}> Detail</Button>
            <Modal isOpen={openModal} onClick={toggle} fade={true}>
                <ModalHeader toggle={toggle}>{detailMovie.Title}</ModalHeader>
                <ModalBody>
                    <img src={detailMovie.Poster} alt={`This is poster of ${movie.Title}`} />
                    <p>Released: {detailMovie.Released}</p>
                    <p>Rated: {detailMovie.imdbRating}</p>
                    <p>Runtime: {detailMovie.Runtime}</p>
                    <p>Genre: {detailMovie.Genre}</p>
                    <p>Director: {detailMovie.Director}</p>
                    <p>Actors: {detailMovie.Actors}</p>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}
export default Detail