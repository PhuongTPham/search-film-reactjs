import React from 'react';
import Disqus from "disqus-react";

function DisComment() {
    const dispusShortName = "review-film"
    const disqusConfig = {
        url: "http://localhost:3000",
        identifier: "article-id",
        title: "Title of Your Article"
    }

    return(
        <div className="article-container">
            <Disqus.DiscussionEmbed
                shortname={dispusShortName}
                config={disqusConfig} />
        </div>
    )
}
export default DisComment

