import React, {useState, useEffect} from 'react';
import Header from "../components/Header";
import Movie from "../components/Movie";
import Search from "../components/Search";
import ClipLoader from "react-spinners/ClipLoader";
import {css} from "@emotion/core";
import '../style/App.css';
import Footer from "../components/Footer";
import DisComment from "../components/dispusComment";
const MOVIE_URL = "http://www.omdbapi.com/?s=harry&apikey=bc1d4985"
const overloading = css `
    display: block;
    border: 4px solid;
    border-color: #0d2167;
    margin: 0 auto;
    text-align: center
`;
function Home() {
    const [loading, setLoading] = useState("true")
    const [movies, setMovies] = useState([])
    const[errorMessage, setErrorMessage] = useState(null)
    useEffect(() => {
        fetch(MOVIE_URL)
            .then(response => response.json())
            .then(jsonResponse => {
                setMovies(jsonResponse.Search);
                setLoading(false);
            });
    }, []);
    const search = searchValue => {
        setLoading(true);
        setErrorMessage(null);
        fetch(`https://www.omdbapi.com/?s=${searchValue}&apikey=bc1d4985`)
            .then(response => response.json())
            .then(jsonResponse => {
                if (jsonResponse.Response === "True") {
                    setMovies(jsonResponse.Search);
                    setLoading(false);
                } else {
                    setErrorMessage(jsonResponse.Error);
                    setLoading(false);
                }
            });
    };
  return (
    <div className="App">
      <header className="App-header">
        <Header text="Welcome! You can find many famous films in my page" />
      </header>
        <Search search={search} />
        <div className="movies">
            {loading && !errorMessage ? (
                <ClipLoader
                    css={overloading}
                    size={300}
                    color={"#0d2167"}
                    loading={loading}
                />
            ) : errorMessage ? (
                <div className="errorMessage">{errorMessage}</div>
            ) : (
                movies.map((movie, index) => (
                    <Movie key={`${index}-${movie.Title}`} movie={movie} />
                ))
            )}react
        </div>
        <DisComment />
    </div>
  );
}

export default Home;
