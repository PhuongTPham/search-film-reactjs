import React from "react";
import { Grid, Button } from "@material-ui/core";
import { Link } from "react-router-dom";

export default function NotFoundPage() {

  return (
    <Grid container >
      <p>
        404 Not Found Page
      </p>
        <Button
          variant="contained"
          color="primary"
          component={Link}
          to="/"
          size="large"
        >
          Back to Home
        </Button>
    </Grid>
  );
}